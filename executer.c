#include "sush.h"

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/wait.h>
#include <stdbool.h>
#include <signal.h>

pid_t child_pid = 0;

// return: if a child was terminated
bool kill_child() {
	if (child_pid != 0) {
		kill(child_pid, SIGINT);
		return true;
	}
	return false;
}

// t: an array of tokens
// n: the size of the array
void execute(struct token t[], int n)
{

	// --- create command from tokens --- //
	bool in_command = true;
	char *command[STR_SIZE];
	bool stdin_found = false;
	FILE *stdin;
	bool stdout_found = false;
	FILE *stdout;

	for (int i = 0; i < n; i++) {
		// command
		if (t[i].type == 0 && in_command) {
			command[i] = t[i].value;
		}
		// redirect operations
		else if (t[i].type == 1) {
			if (in_command) {
				command[i] = NULL;
			}
			in_command = false;

			if (i >= n - 1) {
				fprintf(stderr, "sush: no file specified after redirect operator\n");
				return;
			}
			else if (t[i + 1].type == 1) {
				fprintf(stderr, "sush: cannot have two redirect operators in a row\n");
				return;
			}
			// stdin
			else if (strcmp(t[i].value, "<") == 0) {
				stdin_found = true;
				stdin = fopen(t[i + 1].value, "r");
				if (stdin == NULL) {
					fprintf(stderr, "sush: %s\n", strerror(errno));
					return;
				}
			}
			// stdout
			else if (strcmp(t[i].value, ">") == 0) {
				stdout_found = true;
				stdout = fopen(t[i + 1].value, "w");
			}
		}
	}

	// NULL termination
	command[n] = NULL;

	// --- internal commands --- //
	if (command[0] != NULL) {

		if (strcmp(command[0], "setenv") == 0) {
			if (command[1] != NULL && command[2] != NULL) {
				setvariable(command[1], command[2]);
			}
			else {
				fprintf(stderr, "sush: setenv requires a variable and value\n");
			}
			return;
		}

		else if (strcmp(command[0], "unsetenv") == 0) {
			if (command[1] != NULL) {
				unsetvariable(command[1]);
			}
			else {
				fprintf(stderr, "sush: unsetenv requires a variable\n");
			}
			return;
		}

		else if (strcmp(command[0], "cd") == 0) {
			if (command[1] != NULL) {
				cd(command[1]);
			}
			else {
				cd(NULL);
			}
			return;
		}

		else if (strcmp(command[0], "pwd") == 0) {
			pwd();
			return;
		}

		else if (strcmp(command[0], "exit") == 0) {
			iexit(0);
		}
	}

	pid_t pid = fork();

	// --- child process --- //
	if (pid == 0) {

		if (stdin_found) {
			bool err_found = false;
			int err;
			// set the stdin, everything else is for error checking
			if (dup2(fileno(stdin), STDIN_FILENO) == -1) {
				err = errno;
				err_found = true;
			}

			fclose(stdin);

			if (err_found) {
				fprintf(stderr, "sush: %s\n", strerror(errno));
				exit(-1);
			}
		}
		if (stdout_found) {
			bool err_found = false;
			int err;
			// set the stdout, everything else is for error checking
			if (dup2(fileno(stdout), STDOUT_FILENO) == -1) {
				err = errno;
				err_found = true;
			}

			fclose(stdout);

			if (err_found) {
				fprintf(stderr, "sush: %s\n", strerror(errno));
				exit(-1);
			}
		}

		// execute the command
		execvp(command[0], command);

		// error
		fprintf(stderr, "sush: %s\n", strerror(errno));
		exit(-1);
	}

	// --- parent process --- //
	else {
		
		child_pid = pid;

		if (wait(NULL) == -1) {
			// print error
			fprintf(stderr, "sush: %s\n", strerror(errno));
		}

		child_pid = 0;
	}
}
