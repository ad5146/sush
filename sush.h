#include <stdbool.h>

#ifndef SUSH_H
#define SUSH_H

// maximum string size (+2 for \n and \0)
#define STR_SIZE 1023 + 2

// tokenizer.c
struct token
{
	char value[STR_SIZE];
	int type;	// 0 for normal, 1 for special
};
int tokenize(struct token dest[], char command[]);

// executer.c
void execute(struct token t[], int n);
bool kill_child();

// icommands.c
void pwd();
void iexit();
void cd(char *arg);
void setvariable(char *name, char *value);
void unsetvariable(char *name);

#endif
