#include "stdio.h"
#include "string.h"
#include "sush.h"
#include "stdbool.h"

// dest: the resulting array of tokens
// command: the command string to tokenize
// return: the number of tokens in the resulting array. returns -1 if there is
//         an odd amount of quotes
int tokenize(struct token dest[], char command[])
{
	int token_i = 0;
	bool curr_token_set = false;
	bool in_quotes = false;

	for (int i = 0; i < strlen(command); i++) {

		// quotes
		if (command[i] == '"') {
			in_quotes = !in_quotes;
		}
		// any value within quotes
		else if (in_quotes) {
			if (!curr_token_set) strcpy(dest[token_i].value, (char[1]) {'\0'});

			strcat(dest[token_i].value, (char[2]) {command[i], '\0'});
			dest[token_i].type = 0;

			curr_token_set = true;
		}
		// < and > outside of quotes
		else if (command[i] == '<' || command[i] == '>') {

			if (curr_token_set) token_i++;

			strcpy(dest[token_i].value, (char[2]) {command[i], '\0'});
			dest[token_i].type = 1;

			token_i++;
			curr_token_set = false;
		}
		// spaces outside of quotes
		else if (command[i] == ' ') {
			if (curr_token_set) {
				token_i ++;
				curr_token_set = false;
			}
		}
		// any other character outside of quotes
		else {
			if (!curr_token_set) strcpy(dest[token_i].value, (char[1]) {'\0'});
			
			strcat(dest[token_i].value, (char[2]) {command[i], '\0'});
			dest[token_i].type = 0;

			curr_token_set = true;
		}
	}

	if (in_quotes) return -1;
	if (!curr_token_set) return token_i;
	else return token_i + 1;
}