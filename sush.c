#include "sush.h"

#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
#include <signal.h>

// ctrl+c handler
void intHandler(int sig) {
	if (!kill_child()) exit(0);
}

int main()
{
	signal(SIGINT, intHandler);

	// ------ run .sushrc ------ //
	// --- open .sushrc --- //
	char sushrc_path[STR_SIZE];
	strcpy(sushrc_path, getenv("HOME"));
	strcat(sushrc_path, "/.sushrc");
	FILE *sushrc = fopen(sushrc_path, "r");

	if (sushrc != NULL) {
		
		// --- 1 read line --- //
		char *line = NULL;
		size_t len = 0;
		ssize_t read;
		while ((read = getline(&line, &len, sushrc)) != -1) {

			line[strcspn(line, "\n")] = 0;	// remove tailing newline

			// input size ok
			if (strlen(line) <= STR_SIZE - 2) {

				line[strcspn(line, "\n")] = 0;	// remove tailing newline

				// --- 2 tokenize --- //
				struct token command[STR_SIZE];
				int size = tokenize(command, line);

				// --- 4 execute --- //
				if (size >= 0) {
					execute(command, size);
				}
				else {
					printf("sush: cannot use an odd amound of quotes\n");
				}
			}

			// input size too big
			else {
				fprintf(stderr, "sush: character limit is %d\n", STR_SIZE - 2);
			}
		}

		free(line);
		fclose(sushrc);
	}

	// ------ main loop ------ //
	while (true) {

		// --- 1 prompt --- //

		char prompt[STR_SIZE];
		if (getenv("PS1") != NULL) strcpy(prompt, getenv("PS1"));
		else strcpy(prompt, "$");
		printf("%s ", prompt);

		// --- 2 user input --- //

		char buff[STR_SIZE];
		// ctrl+D -> exit
		if (fgets(buff, STR_SIZE, stdin) == NULL) return 0;

		// input size ok
		if (buff[strlen(buff) - 1] == '\n') {

			buff[strcspn(buff, "\n")] = 0;	// remove tailing newline

			// --- 3 tokenize --- //

			struct token command[STR_SIZE];
			int size = tokenize(command, buff);

			// --- 4 execute --- //

			if (size >= 0) {
				execute(command, size);
			}
			else {
				printf("sush: cannot use an odd amound of quotes\n");
			}
		}

		// input size too big
		else {

			fprintf(stderr, "sush: character limit is %d\n", STR_SIZE - 2);

			bool clearing_fgets = true;
			while (clearing_fgets) {

				if (buff[strlen(buff) - 1] != '\n') {
					fgets(buff, STR_SIZE, stdin);
				}
				else {
					clearing_fgets = false;
				}
			}
		}
	}
}
