OBJS=sush.o tokenizer.o executer.o icommands.o
CC=gcc

all: sush

sush: $(OBJS) sush.h
	$(CC) -o sush $(OBJS)

sush.o: sush.c
	$(CC) -c sush.c

tokenizer.o: tokenizer.c
	$(CC) -c tokenizer.c

executer.o: executer.c
	$(CC) -c executer.c

icommands.o: icommands.c
	$(CC) -c icommands.c

clean:
	rm -f *.o sush
