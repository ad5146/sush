#include "sush.h"
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>


// Prints the current working directory
void pwd()
{
        char cwd[STR_SIZE];

        if (getcwd(cwd, STR_SIZE) == NULL) {
        	fprintf(stderr, "sush: %s\n", strerror(errno));
        }
        else {
        	printf("%s\n", cwd);
        }
}

// Exits the current running process
void iexit()
{
        exit(0);
}

// Changes directory
void cd(char *arg)
{

        if(arg == NULL)                              // If just "cd" is specified, takes user to their home directory.
        {
                chdir(getenv("HOME"));
                return;
        }

        char *p;
        p = strchr(arg, '~');

		if(p != NULL)                    // Checks for the "~" charater, representing home directory then replaces it with the home directory
        {
				p++;	// removes the first character (tilde in this case)
				char path[STR_SIZE];
				strcpy(path, getenv("HOME"));

				if (strlen(p) > 0) {
	                strcat(path, p);
            	}
		
		if(chdir(path) == -1)
		{
			fprintf(stderr, "sush: %s\n", strerror(errno));
		}
        }
	else						// Other cd calls
	{
		if(chdir(arg) == -1)
		{
			fprintf(stderr, "sush: %s\n", strerror(errno));
		}
	}
		
}

// Sets environment variable with given name and value.
void setvariable(char *name, char *value)
{
        setenv(name,value,1);
}

// Removes environment variable with given name
void unsetvariable(char *name)
{
        unsetenv(name);
}

